class Kadai1{
	public static void main(String[] args){
		//Q1
		int number = 5;
		String text = "ああああ";
		boolean flg = true;


		//Q2
		System.out.println(number);
		System.out.println(text);


		//Q3
		int number1 = 10;
		int number2 = 5;

		System.out.println(number1 + number2);//加算
		System.out.println(number1 - number2);//減算
		System.out.println(number1 * number2);//乗算
		System.out.println(number1 / number2);//徐算
		System.out.println(number1 % number2);//割り算のあまり


		//Q4
		int number3 = 103;
		int number4 = 105;
		boolean isOddNumber1 = false;

		if((number3 + number4) %2 == 0){	//2で割ったあまりが0なら
			isOddNumber1 = false;
			System.out.println(isOddNumber1);
		}
		else{
			isOddNumber1 = true;
			System.out.println(isOddNumber1);
		}

		/*
		はじめからfalseが入っているのにfalseを再代入する必要はない
		boolean型はtrueかfalseを扱う
		==、&&、||演算子を扱うと戻り値がboolean型となって帰って来る
		*/
		boolean isOddNumber = (number3 + number4) %2 == 1;
		System.out.println("");
		System.out.println("チェック"+isOddNumber);
		System.out.println("");


		//Q5
		if(isOddNumber == false){
			System.out.println("偶数です");
		}
		else{
			System.out.println("奇数です");
		}


		//Q6
		int number5 = 3;
		int number6 = 5;

		if(number5 * number6 >= 100){	//100以上だったら
			System.out.println("100以上です");
		}
		else if((number5 * number6) %3 == 0){	//3の倍数だったら
			System.out.println("3の倍数です");
		}
		else if((number5 + number6) %5 != 0){	//5の倍数ではなかったら
			System.out.println("5の倍数ではありません");
		}


		//Q7
		if((number5*number6) %3 == 0 && (number5*number6) %5 == 0){
			System.out.println("3と5の倍数です");
		}
		else if((number5*number6) %2 ==0 || (number5*number6) %3 == 0){
			System.out.println("2か3の倍数です");
		}
		if((number5%2 == 0 && number3%2 == 1) || (number3%2 == 0 && number5%2 == 1)){
			System.out.println("偶数と奇数の組み合わせです");
		}
		
		
	}
}